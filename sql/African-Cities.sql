/*
*solution for https://www.hackerrank.com/challenges/african-cities/
*/
select CITY.name
FROM CITY JOIN COUNTRY
ON CITY.CountryCode = COUNTRY.Code
where COUNTRY.continent = 'Africa'