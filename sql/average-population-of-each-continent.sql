/*
*solution for https://www.hackerrank.com/challenges/average-population-of-each-continent
*/
select Country.continent, FLOOR(AVG(CITY.POPULATION))
FROM CITY JOIN COUNTRY
ON CITY.CountryCode = COUNTRY.Code
Group by country.continent