// solution for https://www.hackerrank.com/challenges/minimum-absolute-difference-in-an-array

use std::io;

fn main() {
    let mut input = String::new();
    let mut v: Vec<i32>;

    io::stdin().read_line(&mut input).ok().expect("read error");

    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");
    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect();

    v.sort();
    let mut i = 0;
    let mut small_diff = (v[i] - v[i + 1]).abs();
    while i < v.len() - 1 {
        if (v[i] - v[i + 1]).abs() < small_diff {
            small_diff = (v[i] - v[i + 1]).abs()
        }
        i = i + 1;
    }
    println!("{}", small_diff);
}
