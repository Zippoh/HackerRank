// solution for https://www.hackerrank.com/challenges/marcs-cakewalk

use std::io;

fn main() {
    let mut input = String::new();
    let mut v: Vec<i32>;
    let base = 2 as usize;
    io::stdin().read_line(&mut input).ok().expect("read error");

    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");

    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect();

    v.sort_by(|a, b| b.cmp(a));

    let mut cmpt = 0;
    let miles_nb = v.iter().fold(0, |mut acc, &x| {
        acc = acc + (x as usize * base.pow(cmpt));
        cmpt = cmpt + 1;
        acc
    });
    println!("{}", miles_nb);
}
