package main

import "fmt"

func main() {

	var input int
	fmt.Scan(&input)
	var sum float64
	b := make([]float64, input, 5)

	for j := 0; j < input; j++ {
		fmt.Scan(&b[j])
		sum += b[j]
	}
	fmt.Printf("%d", sum)
}
