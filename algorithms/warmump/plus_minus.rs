use std::io;

struct Value {
    nb_given: i32,
    positive: f64,
    negative: f64,
    zero: f64,
}


fn main() {
    let mut input = String::new();
    let mut number_input: f64;
    let mut data = Value {
        nb_given: 0,
        positive: 0.0,
        negative: 0.0,
        zero: 0.0,
    };
    let mut v: Vec<f64> = Vec::new();


    io::stdin().read_line(&mut input).ok().expect("read error");
    data.nb_given = input.trim().parse::<i32>().unwrap();

    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");

    v.extend(input.split(" ")
                 .map(|x| x.trim().parse::<f64>().unwrap())
                 .collect::<Vec<f64>>()
                 .iter());

    for i in 0..v.len() {
        number_input = v[i];

        if number_input > 0.0 {
            data.positive = data.positive + 1.0;
        } else if number_input < 0.0 {
            data.negative = data.negative + 1.0;
        } else {
            data.zero = data.zero + 1.0
        }
    }

    println!("{}", data.positive / data.nb_given as f64);
    println!("{}", data.negative / data.nb_given as f64);
    println!("{}", data.zero / data.nb_given as f64);
}
