package main

import "fmt"

func main() {
	b := make([]int, 3, 5)
	a := make([]int, 3, 5)

	var resA, resB int32

	fmt.Scanf("%v %v %v", &a[0], &a[1], &a[2])
	fmt.Scanf("%v %v %v", &b[0], &b[1], &b[2])

	for i := 0; i < 3; i++ {

		if a[i] > b[i] {
			println("ok1")

			resA++
		} else if a[i] < b[i] {
			println("ok2")
			resB += 1
		}
	}

	fmt.Printf("%d %d ", resA, resB)
}
