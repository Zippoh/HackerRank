use std::io;

fn main() {
    let mut size_array_str = String::new();
    let mut value_array = String::new();

    let mut v: Vec<i32> = Vec::new();

    io::stdin().read_line(&mut size_array_str).ok().expect("read error");
    let size_array = size_array_str.trim().parse::<i32>().unwrap();

    for i in 0..size_array{
        value_array.clear();
        io::stdin().read_line(&mut value_array).ok().expect("read error");

        v.extend(value_array.split(" ")
                 .map(|x|x.trim().parse::<i32>().unwrap())
                 .collect::<Vec<i32>>().iter());
    }
                     
    let mut res1 = 0;
    let mut res2 = 0;
    let mut cmpt = 0;
    let mut cmpt2 = 1;

    while  cmpt < v.len(){
        res1 = res1 +v[cmpt];
        cmpt = cmpt + (size_array +1) as usize; 
    }

    cmpt = (cmpt2 * size_array - cmpt2) as usize;
    while cmpt2 <=  size_array {
        cmpt = (cmpt2 * size_array - cmpt2) as usize;
        cmpt2 = cmpt2 + 1;        
        res2 = res2 + v[cmpt];
    } 
       println!("{}", (res1 - res2).abs());
}
