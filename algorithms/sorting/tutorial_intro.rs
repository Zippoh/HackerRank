use std::io;

fn main() {
    let mut input = String::new();
    
    io::stdin().read_line(&mut input).ok().expect("read error");
    let value = input.trim().parse::<i32>().unwrap();

    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");
    let bof = input.trim().parse::<i32>().unwrap();

    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");

    let v : Vec<i32> = input.split(" ")
             .map(|x|x.trim().parse::<i32>().unwrap())
             .collect();

    let result = v.iter().position(|&x| x == value);

    match result {
    Some(v) => println!("{}", v),
    None => {}, 
    }

}
