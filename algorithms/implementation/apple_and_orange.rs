// solution for https://www.hackerrank.com/challenges/apple-and-orange

use std::io;

struct Range {
    x1: i32,
    x2: i32,
}

fn main() {
    let mut input = String::new();
    let mut v: Vec<i32> = Vec::new();

    io::stdin().read_line(&mut input).ok().expect("read error");

    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect();

    let home_range = Range {
        x1: v[0],
        x2: v[1],
    };
    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");

    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect();

    let distance_a = Range {
        x1: (home_range.x1 - v[0]).abs(),
        x2: (home_range.x2 - v[0]).abs(),
    };
    let distance_b = Range {
        x1: (home_range.x2 - v[1]).abs(),
        x2: (home_range.x1 - v[1]).abs(),
    };

    //Don't need to read this line
    io::stdin().read_line(&mut input).ok().expect("read error");

    //for the apple
    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");
    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect::<Vec<i32>>()
        .into_iter()
        .filter(|x| x.is_positive())
        .collect();

    println!("{}", get_on_house(v, distance_a));

    //for the orange
    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");
    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect::<Vec<i32>>()
        .into_iter()
        .filter(|x| x.is_negative())
        .collect();


    println!("{}", get_on_house(v, distance_b));
}


fn get_on_house(v: Vec<i32>, distance: Range) -> i32 {
    v.iter().fold(0,
                  |cmp, &x| if x.abs() >= distance.x1 && x.abs() <= distance.x2 {
                      cmp + 1
                  } else {
                      cmp
                  })
}


