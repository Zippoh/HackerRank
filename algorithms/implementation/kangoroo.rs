// solution for https://www.hackerrank.com/challenges/kangaroo

use std::io;
fn main() {
    let mut input = String::new();
    let v: Vec<i32>;

    io::stdin().read_line(&mut input).ok().expect("read error");

    io::stdin().read_line(&mut input).ok().expect("read error");

    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect();

    if v[3] - v[1] != 0 && (v[0] - v[2]) % (v[3] - v[1]) == 0 && (v[0] - v[2]) / (v[3] - v[1]) > 0 {
        println!("YES");;
    } else {
        println!("NO");
    }
}


