use std::io;

const MULTIPLE :i32= 5;

fn main() {
    let mut input = String::new();

    io::stdin().read_line(&mut input).ok().expect("read error");
    let nbr_students = input.trim().parse::<i32>().unwrap();

    for _ in 0..nbr_students{
        input.clear();
        io::stdin().read_line(&mut input).ok().expect("read error");
        let score = input.trim().parse::<i32>().unwrap();

        if  score < 38{
            println!("{}", score);
            continue; 
        } 
        let diff = (score % MULTIPLE - MULTIPLE).abs();
        if diff < 3{
            println!("{}", score + diff );
            continue;
        }else{
            println!("{}", score);
            continue; 
        }
    }
}
