// solution for https://www.hackerrank.com/challenges/bon-appeti

use std::io;
fn main() {
    let mut input = String::new();
    let mut v: Vec<i32>;

    io::stdin().read_line(&mut input).ok().expect("read error");

    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect();

    let index_k = v[1] as usize;
    input.clear();

    io::stdin().read_line(&mut input).ok().expect("read error");
    v = input.split(" ")
        .map(|x| x.trim().parse::<i32>().unwrap())
        .collect();


    input.clear();
    io::stdin().read_line(&mut input).ok().expect("read error");
    let b_charged = input.trim().parse::<i32>().unwrap();


    let b_actual = (v.iter().fold(0, |acc, x| acc + x) - v[index_k]) / 2;
    if b_actual == b_charged {
        println!("Bon Appetit");
    } else {
        println!("{}", b_charged - b_actual);
    }

}

