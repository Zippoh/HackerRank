use std::io;


fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).ok().expect("read error");

    let mut nbr_words =if input.is_empty(){0} else{1};

    for c in input.chars().skip(1) {
        if c as i32 >= 65 && c as i32 <=90{
            nbr_words = nbr_words +1;
        }
    }
    println!("{}", nbr_words);
}
