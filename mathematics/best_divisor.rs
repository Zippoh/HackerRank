// solution for https://www.hackerrank.com/challenges/best-divisor

use std::io;

fn main() {
    let mut input = String::new();
    let number_input: i32;
    let mut best_div: i32 = -1;

    io::stdin().read_line(&mut input).ok().expect("read error");
    number_input = input.trim().parse::<i32>().unwrap();

    //search each divior of the sqrt of the number
    for i in 1..(number_input as f64).sqrt().round() as i32 + 1 {
        if number_input % i == 0 {
            best_div = valiate_number(i, best_div);
            //allow find each divisor of the number
            if i != number_input / i {
                best_div = valiate_number(number_input / i, best_div);
            }
        }
    }
    println!("{}", best_div);
}

//verify if the new number is better than the old one.
fn valiate_number(num: i32, old_num: i32) -> i32 {
    if (sum_digit(num) >= sum_digit(old_num) && num < old_num) ||
       sum_digit(num) > sum_digit(old_num) {
        return num;
    }
    old_num
}

fn sum_digit(mut num: i32) -> i32 {
    let mut sum: i32 = 0;
    while num > 0 {
        sum = sum + num % 10;
        num = (num - num % 10) / 10;
    }
    return sum;
}

