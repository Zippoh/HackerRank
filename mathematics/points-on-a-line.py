#!/bin/python3
#Solution for https://www.hackerrank.com/challenges/points-on-a-line/
import sys

lastX, lastY = -1,-1
isDroit = True
n = int(input().strip())
for a0 in range(n):
    x,y = input().strip().split(' ')
    x,y = [int(x),int(y)]
    if (x == lastX or y == lastY) or (lastX ==-1 and lastY == -1):
        lastX,lastY = x,y
    else:
        isDroit = False
        
print("YES") if isDroit else print("NO")