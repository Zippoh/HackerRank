use std::io;
use std::io::prelude::*;

fn main() {
    let mut input = String::new();
    let mut number_input: i32;

    io::stdin().read_line(&mut input).ok().expect("read error");

    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        number_input = line.unwrap().trim().parse::<i32>().unwrap();
        println!("{}", (number_input) + 1);
    }
}


