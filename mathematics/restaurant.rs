// solution for https://www.hackerrank.com/challenges/restaurant

use std::io;
use std::io::prelude::*;

fn main() {
    let mut input = String::new();
    let mut v: Vec<i32> = Vec::new();

    io::stdin().read_line(&mut input).ok().expect("read error");
    let stdin = io::stdin();

    for line in stdin.lock().lines() {
        v.extend(line.unwrap()
                     .split(" ")
                     .map(|x| x.trim().parse::<i32>().unwrap())
                     .collect::<Vec<i32>>()
                     .iter());

        let gcd = gcd(v[0], v[1]);

        let res = (v[0] / gcd) * (v[1] / gcd);
        println!("{}", res);
        v.clear();
    }
}

fn gcd(a: i32, b: i32) -> i32 {
    if b == 0 {
        return a;
    } else {
        return gcd(b, a % b);
    }
}


