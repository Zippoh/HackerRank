use std::io;
use std::io::prelude::*;

fn main() {
    let mut input = String::new();
    let mut number_input: i64;
    let base10: i64 = 10;
    let modulo: i64 = base10.pow(9) + 7;
    let mut result: i64;

    io::stdin().read_line(&mut input).ok().expect("read error");

    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        number_input = line.unwrap().trim().parse::<i64>().unwrap();

        result = big_expo(number_input, 2, modulo);
        println!("{}", result);
    }
}

fn big_expo(base: i64, mut expo: i32, modulo: i64) -> i64 {
    let mut x = 1;
    let mut power = base % modulo;
    while expo > 0 {
        if expo % 2 == 0 {
            expo = expo / 2;
        } else {
            expo = (expo - 1) / 2;
            x = (x * power) % modulo
        }
        power = (power * power) % modulo;
    }
    x
}
